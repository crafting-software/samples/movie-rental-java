package katas.movierental;

public final class Duration implements Comparable<Duration> {
    private int value;

    public static Duration zero() {
        return new Duration(0);
    }

    public static Duration days(int value) {
        return new Duration(value);
    }

    public static Duration add(Duration left, Duration right) {
        return days(left.value + right.value);
    }

    public static Duration sub(Duration left, Duration right) {
        return days(Math.max(left.value - right.value, 0));
    }

    public static Duration max(Duration left, Duration right) {
        return days(Math.max(left.value, right.value));
    }

    public static Duration min(Duration left, Duration right) {
        return days(Math.min(left.value, right.value));
    }

    public Duration(int value) {
        this.value = value;
    }

    public boolean isZero() {
        return value == 0;
    }

    public boolean isNotZero() {
        return value != 0;
    }

    @Override
    public String toString() {
        return "Duration: " + value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return value == ((Duration) o).value;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(value);
    }

    @Override
    public int compareTo(Duration o) {
        return Integer.compare(value, o.value);
    }

    public int value() {
        return value;
    }
}
