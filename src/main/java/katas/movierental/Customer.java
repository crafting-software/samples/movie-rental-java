package katas.movierental;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private final String _name;
    private final List<Rental> _rentals = new ArrayList<>();

    public Customer(String name) {
        _name = name;
    }

    public void addRental(Rental arg) {
        _rentals.add(arg);
    }

    public String getName() {
        return _name;
    }

    public String statement() {
        // add header lines
        StringBuilder result = new StringBuilder("Rental Record for " + getName() + "\n");
        for (Rental each : _rentals) {
            // show figures for this rental
            result.append("\t").append(each.title()).append("\t").append(each.charge().render()).append("\n");
        }
        // add footer lines
        result.append("Amount owed is ").append(totalAmount().render()).append("\n");
        result.append("You earned ").append(frequentRenterPoints().render()).append(" frequent renter points");
        return result.toString();
    }

    private FrequentRenterPoint frequentRenterPoints() {
        return _rentals.stream().map(Rental::frequentRenterPoints).reduce(FrequentRenterPoint.of(0), FrequentRenterPoint::add);
    }

    private Price totalAmount() {
        return _rentals.stream().map(Rental::charge).reduce(Price.euro(0), Price::add);
    }
}
