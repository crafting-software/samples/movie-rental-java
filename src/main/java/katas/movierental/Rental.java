package katas.movierental;

/**
 * The rental class represents a customer renting a movie.
 */
public class Rental {
    private final Movie _movie;
    private final int _daysRented;

    public Rental(Movie movie, int daysRented) {
        _movie = movie;
        _daysRented = daysRented;
    }

    public String title() {
        return _movie.getTitle();
    }

    public Price charge() {
        return _movie.chargeFor(_daysRented);
    }

    public FrequentRenterPoint frequentRenterPoints() {
        return _movie.frequentRenterPointsFor(_daysRented);
    }
}
