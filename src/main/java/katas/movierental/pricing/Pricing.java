package katas.movierental.pricing;

import io.vavr.collection.List;
import katas.movierental.Duration;
import katas.movierental.Price;

import static java.util.stream.Collectors.joining;


public final class Pricing {
    private final List<Rule> rules;

    public static FlatRuleBuilder costs(final Price price) {
        return new FlatRuleBuilder(price);
    }

    public static Pricing pricing(RuleBuilder builder) {
        return new Pricing(List.of(builder.build()));
    }

    public Pricing then(RuleBuilder builder) {
        return new Pricing(rules.append(builder.build()));
    }

    private Pricing(List<Rule> rules) {
        this.rules = rules;
    }

    public Price chargeFor(Duration duration) {
        return new PriceCalculator(rules, duration).total();
    }

    public String describe() {
        return rules.map(Rule::describe).collect(joining(" then "));
    }

    public String explain(Duration duration) {
        return new PriceCalculator(rules, duration).audit();
    }
}
