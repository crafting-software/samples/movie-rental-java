package katas.movierental.pricing;

import katas.movierental.Duration;
import katas.movierental.Price;

public final class FlatRuleBuilder implements RuleBuilder {
    private final Price price;

    FlatRuleBuilder(Price price) {
        this.price = price;
    }

    public ProportionalRuleBuilder perDay() {
        return new ProportionalRuleBuilder(price);
    }

    public LimitedDurationRuleBuilder for_(Duration duration) {
        return new LimitedDurationRuleBuilder(duration, this);
    }

    public Rule build() {
        return new FlatRule(price);
    }
}
