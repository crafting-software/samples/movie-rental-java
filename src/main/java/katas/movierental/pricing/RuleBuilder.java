package katas.movierental.pricing;

public interface RuleBuilder {
    Rule build();
}
