package katas.movierental.pricing;

import katas.movierental.Duration;

public final class LimitedDurationRuleBuilder implements RuleBuilder {
    private final Duration duration;
    private final RuleBuilder inner;

    LimitedDurationRuleBuilder(Duration duration, RuleBuilder inner) {
        this.duration = duration;
        this.inner = inner;
    }

    public Rule build() {
        return new LimitedDurationRule(duration, inner.build());
    }
}
