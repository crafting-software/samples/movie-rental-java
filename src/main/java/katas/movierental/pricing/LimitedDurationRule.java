package katas.movierental.pricing;

import katas.movierental.Duration;

final class LimitedDurationRule implements Rule {
    private final Duration duration;
    private final Rule inner;

    LimitedDurationRule(Duration duration, Rule inner) {
        this.duration = duration;
        this.inner = inner;
    }

    public RuleResult apply(Duration duration) {
        final Duration consumed = Duration.min(duration, this.duration);
        final RuleResult result = inner.apply(consumed);
        return new RuleResult(result.price(), Duration.sub(duration, consumed));
    }

    public String describe() {
        return inner.describe() + " for " + duration.value() + " day(s)";
    }
}
