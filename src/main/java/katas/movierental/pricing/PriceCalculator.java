package katas.movierental.pricing;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import io.vavr.collection.List;
import katas.movierental.Duration;
import katas.movierental.Price;


final class PriceCalculator {
    private final java.util.List<String> audit = Lists.newArrayList();
    private boolean overflow = false;
    private Price total;

    PriceCalculator(List<Rule> rules, Duration duration) {
        Duration remaining = duration;
        total = Price.euro(0);
        for (Rule rule : rules) {
            RuleResult result = rule.apply(remaining);
            total = total.add(result.price());
            remaining = result.remaining();
            audit.add("rule [" + rule.describe() + "]: price=" + result.price() + ", total=" + total + ", remaining=" + remaining.value());
            if (remaining.isZero()) {
                return;
            }
        }

        if (remaining.isNotZero()) {
            audit.add("overflow of " + remaining.value());
            overflow = true;
        }
    }

    String audit() {
        return Joiner.on("; ").join(audit);
    }

    Price total() {
        if (overflow) {
            throw new Overflow();
        }
        return total;
    }
}
