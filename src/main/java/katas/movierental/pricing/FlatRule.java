package katas.movierental.pricing;

import katas.movierental.Duration;
import katas.movierental.Price;

final class FlatRule implements Rule {
    private final Price price;

    FlatRule(Price price) {
        this.price = price;
    }

    public RuleResult apply(Duration duration) {
        return new RuleResult(price, Duration.zero());
    }

    public String describe() {
        return "cost " + price;
    }
}
