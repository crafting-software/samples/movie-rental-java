package katas.movierental.pricing;

import katas.movierental.Duration;
import katas.movierental.Price;

public record RuleResult(Price price, Duration remaining) {
}
