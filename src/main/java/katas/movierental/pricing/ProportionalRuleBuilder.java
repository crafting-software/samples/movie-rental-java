package katas.movierental.pricing;

import katas.movierental.Duration;
import katas.movierental.Price;

public final class ProportionalRuleBuilder implements RuleBuilder {
    private final Price price;

    ProportionalRuleBuilder(Price price) {
        this.price = price;
    }

    public LimitedDurationRuleBuilder for_(Duration duration) {
        return new LimitedDurationRuleBuilder(duration, this);
    }

    public Rule build() {
        return new ProportionalRule(price);
    }
}
