package katas.movierental.pricing;

import katas.movierental.Duration;

public interface Rule {
    RuleResult apply(Duration duration);

    String describe();
}
