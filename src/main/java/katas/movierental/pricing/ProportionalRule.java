package katas.movierental.pricing;

import katas.movierental.Duration;
import katas.movierental.Price;

final class ProportionalRule implements Rule {
    private final Price price;

    ProportionalRule(Price price) {
        this.price = price;
    }

    public RuleResult apply(Duration duration) {
        return new RuleResult(price.mul(duration.value()), Duration.zero());
    }

    public String describe() {
        return "cost " + price + " per day";
    }
}
