package katas.movierental;

import com.google.common.base.Objects;

import java.util.Locale;

public class Price {
    private final double value;

    public static Price euro(final double value) {
        return new Price(value);
    }

    private Price(double value) {
        this.value = value;
    }

    public double value() {
        return value;
    }

    public Price add(Price right) {
        return euro(value + right.value);
    }

    public Price mul(double factor) {
        return euro(value * factor);
    }

    public String render() {
        return String.format(Locale.ENGLISH, "%.1f", value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return Double.compare(price.value, value) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }

    @Override
    public String toString() {
        return "€" + value;
    }
}
