package katas.movierental;

public class Movie {
    private final String _title;
    private final PriceCode _priceCode;

    public Movie(String title, PriceCode priceCode) {
        _title = title;
        _priceCode = priceCode;
    }

    public String getTitle() {
        return _title;
    }

    public Price chargeFor(int daysRented) {
        return _priceCode.chargeFor(daysRented);
    }

    public FrequentRenterPoint frequentRenterPointsFor(int daysRented) {
        return _priceCode.frequentRenterPointsFor(daysRented);
    }
}
