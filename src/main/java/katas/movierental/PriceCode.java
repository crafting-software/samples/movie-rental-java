package katas.movierental;

import katas.movierental.pricing.Pricing;

import static katas.movierental.Duration.days;
import static katas.movierental.Price.euro;
import static katas.movierental.pricing.Pricing.costs;
import static katas.movierental.pricing.Pricing.pricing;

public enum PriceCode {

    CHILDREN(pricing(costs(euro(1.5)).for_(days(3))).then(costs(euro(1.5)).perDay())) {
        public FrequentRenterPoint frequentRenterPointsFor(int daysRented) {
            return FrequentRenterPoint.of(1);
        }
    },
    NEW_RELEASE(pricing(costs(euro(3)).perDay())) {
        public FrequentRenterPoint frequentRenterPointsFor(int daysRented) {
            return (daysRented > 1) ? FrequentRenterPoint.of(2) : FrequentRenterPoint.of(1);
        }
    },
    REGULAR(pricing(costs(euro(2)).for_(days(2))).then(costs(euro(1.5)).perDay())) {
        public FrequentRenterPoint frequentRenterPointsFor(int daysRented) {
            return FrequentRenterPoint.of(1);
        }
    };

    final private Pricing pricing;

    PriceCode(Pricing pricing) {
        this.pricing = pricing;
    }

    public Price chargeFor(int daysRented) {
        return this.pricing.chargeFor(days(daysRented));
    }

    public abstract FrequentRenterPoint frequentRenterPointsFor(int daysRented);
}
