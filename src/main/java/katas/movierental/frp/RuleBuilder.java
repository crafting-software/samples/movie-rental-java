package katas.movierental.frp;

public interface RuleBuilder {
    Rule build();
}
