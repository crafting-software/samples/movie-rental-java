package katas.movierental.frp;

import katas.movierental.Duration;
import katas.movierental.FrequentRenterPoint;

final class FlatRule implements Rule {
    private final FrequentRenterPoint points;

    FlatRule(FrequentRenterPoint points) {
        this.points = points;
    }

    public RuleResult apply(Duration duration) {
        return new RuleResult(points, Duration.zero());
    }

    public String describe() {
        return "earn " + points;
    }
}
