package katas.movierental.frp;

import katas.movierental.Duration;
import katas.movierental.FrequentRenterPoint;

public record RuleResult(FrequentRenterPoint points, Duration remaining) {
}
