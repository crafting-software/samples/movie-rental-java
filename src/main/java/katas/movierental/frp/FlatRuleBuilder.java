package katas.movierental.frp;

import katas.movierental.Duration;
import katas.movierental.Price;
import katas.movierental.pricing.LimitedDurationRuleBuilder;
import katas.movierental.pricing.ProportionalRuleBuilder;
import katas.movierental.pricing.Rule;
import katas.movierental.pricing.RuleBuilder;

public final class FlatRuleBuilder implements RuleBuilder {
    private final Price price;

    FlatRuleBuilder(Price price) {
        this.price = price;
    }

    public LimitedDurationRuleBuilder for_(Duration duration) {
        return new LimitedDurationRuleBuilder(duration, this);
    }

    public Rule build() {
        return new FlatRule(price);
    }
}
