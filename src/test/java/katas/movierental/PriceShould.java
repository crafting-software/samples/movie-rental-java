package katas.movierental;

import org.junit.jupiter.api.Test;

import static katas.movierental.Price.euro;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PriceShould {
    @Test
    public void be_comparable() {
        assertEquals(euro(2.0), euro(2.0));
    }

    @Test
    public void render() {
        assertEquals("2.0", euro(2.0).render());
    }

    @Test
    public void add() {
        assertEquals(euro(5.0), euro(2.0).add(euro(3.0)));
    }

    @Test
    public void mul_with_constant() {
        assertEquals(euro(20.0), euro(2.0).mul(10));
    }
}
