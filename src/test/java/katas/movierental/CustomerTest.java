package katas.movierental;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CustomerTest {
    public static final Movie star_war = new Movie("Star wars I", PriceCode.REGULAR);
    public static final Movie bambi = new Movie("Bambi", PriceCode.CHILDREN);
    public static final Movie modern = new Movie("Modern Film", PriceCode.NEW_RELEASE);


    @Test
    public void test_harness() {
        final Customer customer = new Customer("Jhon Doe");
        customer.addRental(new Rental(star_war, 5));
        customer.addRental(new Rental(star_war, 2));
        customer.addRental(new Rental(bambi, 7));
        customer.addRental(new Rental(bambi, 3));
        customer.addRental(new Rental(modern, 3));
        customer.addRental(new Rental(modern, 6));

        Assertions.assertEquals("""
                Rental Record for Jhon Doe
                	Star wars I	6.5
                	Star wars I	2.0
                	Bambi	7.5
                	Bambi	1.5
                	Modern Film	9.0
                	Modern Film	18.0
                Amount owed is 44.5
                You earned 8 frequent renter points""", customer.statement());
    }
}