package katas.movierental;

import org.junit.jupiter.api.Test;

import static katas.movierental.PriceCode.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PriceCodeUnitTest {
    @Test
    public void regular_movie_should_charge_flat_when_less_than_3_days() {
        assertEquals(euro(2.0), REGULAR.chargeFor(days(1)));
        assertEquals(euro(2.0), REGULAR.chargeFor(days(2)));
    }

    @Test
    public void regular_movie_should_charge_proportional_when_more_than_2_days() {
        assertEquals(euro(3.5), REGULAR.chargeFor(days(3)));
        assertEquals(euro(5.0), REGULAR.chargeFor(days(4)));
    }

    @Test
    public void regular_movie_should_give_one_frequent_renter_point() {
        assertEquals(frp(1), REGULAR.frequentRenterPointsFor(days(1)));
        assertEquals(frp(1), REGULAR.frequentRenterPointsFor(days(2)));
        assertEquals(frp(1), REGULAR.frequentRenterPointsFor(days(3)));
        assertEquals(frp(1), REGULAR.frequentRenterPointsFor(days(10)));
    }

    @Test
    public void children_movie_should_charge_flat_when_less_than_4_days() {
        assertEquals(euro(1.5), CHILDREN.chargeFor(days(1)));
        assertEquals(euro(1.5), CHILDREN.chargeFor(days(2)));
        assertEquals(euro(1.5), CHILDREN.chargeFor(days(3)));
    }

    @Test
    public void children_movie_should_charge_proportional_when_more_than_3_days() {
        assertEquals(euro(3.0), CHILDREN.chargeFor(days(4)));
        assertEquals(euro(4.5), CHILDREN.chargeFor(days(5)));
    }

    @Test
    public void children_movie_should_give_one_frequent_renter_point() {
        assertEquals(frp(1), CHILDREN.frequentRenterPointsFor(days(1)));
        assertEquals(frp(1), CHILDREN.frequentRenterPointsFor(days(2)));
        assertEquals(frp(1), CHILDREN.frequentRenterPointsFor(days(3)));
        assertEquals(frp(1), CHILDREN.frequentRenterPointsFor(days(10)));
    }

    @Test
    public void new_release_movie_should_charge_proportional() {
        assertEquals(euro(3.0), NEW_RELEASE.chargeFor(days(1)));
        assertEquals(euro(6.0), NEW_RELEASE.chargeFor(days(2)));
        assertEquals(euro(9.0), NEW_RELEASE.chargeFor(days(3)));
    }

    @Test
    public void new_release_movie_should_give_one_frequent_renter_point_when_less_than_2_days() {
        assertEquals(frp(1), NEW_RELEASE.frequentRenterPointsFor(days(1)));
    }

    @Test
    public void new_release_movie_should_give_two_frequent_renter_points_when_more_than_1_days() {
        assertEquals(frp(2), NEW_RELEASE.frequentRenterPointsFor(days(2)));
        assertEquals(frp(2), NEW_RELEASE.frequentRenterPointsFor(days(10)));
    }

    private static Price euro(double value) {
        return Price.euro(value);
    }

    private static FrequentRenterPoint frp(int value) {
        return FrequentRenterPoint.of(value);
    }

    private static int days(int value) {
        return value;
    }

}