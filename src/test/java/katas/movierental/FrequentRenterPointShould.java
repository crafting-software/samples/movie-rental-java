package katas.movierental;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FrequentRenterPointShould {
    @Test
    public void be_comparable() {
        assertEquals(FrequentRenterPoint.of(2), FrequentRenterPoint.of(2));
    }

    @Test
    public void render() {
        assertEquals("2", FrequentRenterPoint.of(2).render());
    }

    @Test
    public void add() {
        assertEquals(FrequentRenterPoint.of(5), FrequentRenterPoint.of(2).add(FrequentRenterPoint.of(3)));
    }

}