package katas.movierental;

import org.junit.jupiter.api.Test;

import static katas.movierental.Duration.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DurationShould {

    @Test
    public void render_to_string() {
        assertEquals("Duration: 0", zero().toString());
        assertEquals("Duration: 5", days(5).toString());
    }

    @Test
    public void be_equal() {
        assertEquals(zero(), zero());
        assertEquals(days(5), days(5));
    }

    @Test
    public void be_comparable() {
        assertEquals(-1, zero().compareTo(days(1)));
        assertEquals(-1, days(5).compareTo(days(10)));
    }

    @Test
    public void be_addable() {
        assertEquals(days(7), add(days(3), days(4)));
        assertEquals(days(7), add(zero(), days(7)));
    }

    @Test
    public void be_substractable() {
        assertEquals(days(3), sub(days(7), days(4)));
        assertEquals(zero(), sub(days(4), days(7)));
    }

    @Test
    public void check_to_zero() {
        assertTrue(zero().isZero());
        assertTrue(days(4).isNotZero());
    }
}
